#!/usr/bin/env bash

echo "Install Composer"
composer install --no-interaction --no-suggest -vvv

ROOT=/var/docker

echo "=========================================================================="
php -v
echo "=========================================================================="
php-fpm
