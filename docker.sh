#!/usr/bin/env bash

# -----------------------------------------
# Init Constants
# -----------------------------------------
ACTION=$1; shift # Action is already saved, so we can shift first arg

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
GAMEE_DIR="$DIR/../"

_PHPSTAN='-d memory_limit=512M vendor/phpstan/phpstan/bin/phpstan'
_CS='vendor/bin/phpcs --standard=vendor/gamee/php-code-checker-rules/ruleset.xml
--extensions=php,phpt --tab-width=4 --ignore=temp -sp src'

# -----------------------------------------
# Config Variables
# -----------------------------------------

BASE_IP=172.21.130
PROJECT_NAME=dotykacka

NGINX_IP=172.21.130.20
KIBANA_IP=172.21.130.66
DB_IP=172.21.130.30
PHP_IP=172.21.130.10

# ifconfig docker0 | awk '/inet /{print substr($2,6)}'
HOST_IP=172.17.0.1

# -----------------------------------------
# Docker tools checkout
# -----------------------------------------

source $DIR/_injector.sh

# -----------------------------------------
# Resolvers
# -----------------------------------------

function resolve_workdir() {
	if [[ $1 == "is" ]];	then echo '/var/www/is'
    elif [[ $1 == "pay" ]];	then echo '/var/www/pay'
	else echo "undefined workdir realm"; exit 1
	fi
}

cd $DIR

# -----------------------------------------
# Actions
# -----------------------------------------

if [[ $ACTION == "run" ]]; then

	set -e

	injector php root
	injector node root


	(docker build $DIR/php --build-arg DOCKER_UID=$UID --tag $PROJECT_NAME-php || exit 1) \
	& (docker build $DIR/db --build-arg DOCKER_UID=$UID --tag $PROJECT_NAME-db) \
	& (docker build $DIR/node --build-arg DOCKER_UID=$UID --tag $PROJECT_NAME-node) \
	& wait && echo 'Builds done'; echo || exit 1


	docker-compose -f $DIR/docker-compose.yml stop; echo

	docker-compose -f $DIR/docker-compose.yml up -d --remove-orphans

	docker network disconnect intranet $PROJECT_NAME-nginx > /dev/null || true
	docker network connect intranet $PROJECT_NAME-nginx --ip $NGINX_IP --alias pay.dotykacka.docker

	docker network disconnect intranet $PROJECT_NAME-db > /dev/null || true
	docker network connect intranet $PROJECT_NAME-db --ip $DB_IP


	docker-compose -f $DIR/docker-compose.yml logs --follow --tail="30"

elif [[ $ACTION == "stop" ]]; then
	docker-compose -f $DIR/docker-compose.yml stop


elif [[ $ACTION == "xdebug" ]]; then
	ifconfig docker0 | grep inet


#elif [[ $ACTION == "attach-ds" ]]; then
#	_bindVendor admin
#	_bindVendor api

elif [[ $ACTION == "prune" ]]; then
	docker-compose -f $DIR/docker-compose.yml stop
	docker-compose -f $DIR/docker-compose.yml rm --force

elif [[ $ACTION == "logs" ]]; then
	docker-compose -f $DIR/docker-compose.yml logs $*


elif [[ $ACTION == "is" ]] \
 || [[ $ACTION == "pay" ]] ; then

	SECOND_ACTION=$1; shift

	WORKDIR=$(resolve_workdir $ACTION)

	# Workdir + Container shortuct
	WC="-w $WORKDIR dotykacka-php"
	WCF="-w $WORKDIR dotykacka-no-xdebug"

	if [[ $SECOND_ACTION == "composer" ]]; then
		docker exec -it $WCF composer $* -v

	elif [[ $SECOND_ACTION == "composer-notty" ]]; then
		docker exec $WCF composer $* -vvv

	elif [[ $SECOND_ACTION == "ci" ]]; then
		docker exec -it $WCF composer -vvv install $*

	elif [[ $SECOND_ACTION == "cu" ]]; then
		docker exec -it $WCF composer -vvv update $*

	elif [[ $SECOND_ACTION == "console" ]]; then
		docker exec -it $WCF php bin/console $*

	elif [[ $SECOND_ACTION == "php" ]]; then
		docker exec -t $WC php $*

	elif [[ $SECOND_ACTION == 'node' ]]; then
		docker exec -it $PROJECT_NAME-node $*

	elif [[ $SECOND_ACTION == 'yarn' ]]; then
		docker exec -it $PROJECT_NAME-node yarn $*

	elif [[ $SECOND_ACTION == "stan" ]]; then
		docker exec -t $WCF php $_PHPSTAN analyse src -c phpstan.neon --level 7 $*

	elif [[ $SECOND_ACTION == 'tester' ]]; then
		SOURCE="$@"

		if [[ $* == '' ]]; then SOURCE="tests/UnitTest"; fi

		docker exec -i $WC php vendor/bin/tester $SOURCE -C --colors 1
		exit $?

#	elif [[ $SECOND_ACTION == 'update-ds' ]]; then
#		docker exec -i $WC bash -c "rm -rf vendor/backend/data-storage"
#		docker exec -it $WC composer update -vvv
#
#		docker exec -it $WC composer require backend/data-storage:^$1 -vvv --update-with-all-dependencies

	elif [[ $SECOND_ACTION == "cs" ]]; then
		docker exec -it $WCF composer app:phpcs

	elif [[ $SECOND_ACTION == "cbf" ]]; then
		PATHS="$@"

		if [[ $PATHS == '' ]]; then
			PATHS='src tests'
		fi
		docker exec -i $WCF vendor/bin/phpcbf \
			--standard=vendor/dotykacka/php-code-checker-rules/ruleset.xml \
			--extensions=php,phpt \
			--tab-width=4 \
			--ignore=temp -sp \
			$PATHS

	elif [[ $SECOND_ACTION == "precommit-hook" ]]; then
		ORIGIN="devel"

		if [[ $ACTION == "api-ds" ]] || [[ $ACTION == "bots-ds" ]] || [[ $ACTION == "admin-ds" ]]; then
			ORIGIN="master"
		fi

		MOD_FILES=$(docker exec -i $WCF git diff --diff-filter=ACMRT --name-status origin/$ORIGIN | cut -f2 | grep .php)

		docker exec -i $WCF vendor/bin/phpcbf \
			--standard=vendor/dotykacka/php-code-checker-rules/ruleset.xml \
			--extensions=php,phpt \
			--tab-width=4 \
			--ignore=temp -sp \
			$MOD_FILES

		docker exec -i $WCF git add .

	elif [[ $SECOND_ACTION != '' ]]; then
		docker exec -it $WC $SECOND_ACTION $*

	else echo 'No action'; fi

elif [[ $ACTION == "redis" ]]; then
	SECOND_ACTION=$1; shift

	if [[ $SECOND_ACTION == "flush" ]]; then
		docker exec -t $PROJECT_NAME-redis redis-cli flushall $*
	else
		docker exec -t $PROJECT_NAME-redis redis-cli "$SECOND_ACTION" "$@"
	fi
else
	echo "Unknown action '$ACTION'"
fi

exit $?
